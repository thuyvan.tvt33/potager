const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

var corsOptions = {
    origin: ["http://localhost:4200", "http://localhost:4000"]
}

app.use(cors(corsOptions));

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extendede: true }));

const PORT = process.env.PORT || 8000;
app.listen(PORT, () => {
    console.log(`Server is started and listening on port ${PORT}.`);
});

app.get("/", function (request, response) {
    response.send("Hello Node.js");
});

const db = require("./app/models/plante");

require("./app/routes/plante.route")(app);


