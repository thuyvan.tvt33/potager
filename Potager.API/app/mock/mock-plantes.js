module.exports = [ 
{
    id: 1,
    nom: 'Fraise',
    type: 'Fruit',
    etat: 'Planté',
    dateMAJ: new Date(),
    description: 'La fraise est un fruit rouge issu des fraisiers, espèces de plantes herbacées appartenant au genre Fragaria, dont plusieurs variétés sont cultivées.'
},
{
    id: 2,
    nom: 'Pomme de terre',
    type: 'Légume',
    etat: 'Planté',
    dateMAJ: new Date(),
    description: 'La pomme de terre ou patate, est un tubercule comestible produit par l’espèce Solanum tuberosum, appartenant à la famille des solanacées.'
},
{
    id: 3,
    nom: 'Aubergine',
    type: 'Légume',
    etat: 'Planté',
    dateMAJ: new Date(),
    description: 'L’aubergine (Solanum melongena L.) est une espèce de plantes dicotylédones de la famille des Solanaceae, originaire d\'Asie. '
},
{
    id: 4,
    nom: 'Carotte',
    type: 'Légume',
    etat: 'Planté',
    dateMAJ: new Date(),
    description: 'La carotte, Daucus carota subsp. sativus, est une plante bisannuelle de la famille des apiacées (aussi appelées ombellifères), largement cultivée pour sa racine pivotante charnue, comestible, de couleur généralement orangée, consommée comme légume.'
},
{
    id: 5,
    nom: 'Framboise',
    type: 'Fruit',
    etat: 'Planté',
    dateMAJ: new Date(),
    description: 'La framboise est un fruit rouge issu du framboisier (Rubus idaeus), un arbrisseau de la famille des rosacées.'
},
{
    id: 6,
    nom: 'Epinards',
    type: 'Légume',
    etat: 'Planté',
    dateMAJ: new Date(),
    description: 'L\'épinard (Spinacia oleracea) est une plante potagère, annuelle ou bisannuelle, de la famille des Chenopodiaceae ou des Amaranthaceae selon les classifications.'
},
{
    id: 7,
    nom: 'Myrtille',
    type: 'Fruit',
    etat: 'Planté',
    dateMAJ: new Date(),
    description: 'Les myrtilles sont des fruits rouges produits par diverses espèces du genre Vaccinium (famille des Ericaceae).'
},
{
    id: 8,
    nom: 'Concombre',
    type: 'Légume',
    etat: 'Planté',
    dateMAJ: new Date(),
    description: 'Le concombre (Cucumis sativus) est une plante potagère herbacée, rampante, de la même famille que la calebasse africaine, le melon ou la courge (famille des Cucurbitacées).'
},
{
    id: 9,
    nom: 'Betterarve',
    type: 'Légume',
    etat: 'Planté',
    dateMAJ: new Date(),
    description: 'La betterave, Beta vulgaris subsp. vulgaris, est une sous-espèce de plantes de la famille des Amaranthaceae, cultivées pour leurs racines charnues, et utilisées pour la production du sucre, comme légume dans l\'alimentation humaine, comme plantes fourragères, et plus récemment comme carburant avec le bioéthanol.'
},
{
    id: 10,
    nom: 'Figue',
    type: 'Fruit',
    etat: 'Planté',
    dateMAJ: new Date(),
    description: 'La figue est le fruit du figuier commun (Ficus carica) un arbre de la famille des moracées, emblème du bassin méditerranéen où il est cultivé depuis des millénaires. '
},
];
