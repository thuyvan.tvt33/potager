var PLANTES = require("../mock/mock-plantes");


//Récupérer toutes les plantes
module.exports.getAll = function() {
    return Promise.resolve(PLANTES);
}

//Récupérer une plante grâce à son id
module.exports.getById = function(id) {
    var idNum = Number(id);
    return Promise.resolve(PLANTES.filter(x => x.id === idNum)[0]);
}

//Créer une nouvelle plante
module.exports.create = function(plante) {
    plante.id = PLANTES.length + 1;
    plante.dateMAJ = new Date();
    plante.etat = 'Planté';
    PLANTES.push(plante);
    return Promise.resolve(plante);
}


//Modifier une plante grâce à son id
module.exports.update = function(plante) {
    for (var t = 0; t < PLANTES.length; t++) {
        if (PLANTES[t].id == plante.id) {
            PLANTES[t] = plante;
            return Promise.resolve(1);
        }
    }
    return Promise.reject("introuvable");
}

//Supprimer une plante grâce à son id
module.exports.delete = function(id) {
    for (var t = 0; t < PLANTES.length; t++) {
        if (PLANTES[t].id == id) {
            PLANTES.splice(t, 1);
            return Promise.resolve(1);
        }
    }
    return Promise.reject("introuvable");
}
