// const db = require("../models");
// const Plante = db.Plante;
const Plante = require("../models/plante");

//Créer une nouvelle plante
exports.create = (req, res) => {
    //Valider la requête
    if(!req.body.nom) {
        res.status(400).send({
            message: "Le contenu ne peut être vide"
        });
        return;
    }

    //Créer une plante
    const plante = {
    nom: req.body.nom,
    type: req.body.type,
    description: req.body.description
    };

    //Enregistrer la plante
    Plante.create(plante)
    .then(data => {
        res.send(data);
    })
    .catch(err => {
        res.status(500).send({
            message:
            err.message || "Une erreur est survenue lors de la création de la plante."
        });
    });
};


//Récupérer toutes les plantes
exports.getAll = (req, res) => {

    Plante.getAll().then(data => {
        res.send(data);
    })
    .catch(err => {
        res.status(500).send({
            message:
            err.message || "Une erreur est survenue lors de la récupération des plantes."
        });
    });
};

//Récupérer une plante grâce à son id
exports.getById = (req, res) => {
    const id = req.params.id;
    Plante.getById(id)
    .then(data => {
        res.send(data);
    })
    .catch(err => {
        res.status(500).send({
            message: "Une erreur est survenue avec l'ID:" + id
        });
    });
};


//Modifier une plante grâce à son id
exports.update = (req, res) => {
    const id = req.body.id;

    Plante.update(req.body)
    .then(num => {
        if(num == 1){
            res.send({
                message: "La plante a été mise à jour."
            });
        } else {
            res.send({
                message: "Impossible de mettre à jour la plante avec l'id="+ id
            });
        }
    })
    .catch(err => {
        res.status(500).send({
            message: "Une erreur est survenue lors de la mise à jour de la plante avec l'id=" + id
        });
    });
};


//Supprimer une plante grâce à son id
exports.delete = (req, res) => {
    const id = req.params.id;

    Plante.delete(id)
    .then(num => {
        if(num == 1){
            res.send({
                message: "La plante a été supprimée."
            });
        } else {
            res.send({
                message: "Une erreur est survenue lors de la suppression de la plante avec l'id=" + id
            });
        };
    })
    .catch(err => {
        res.status(500).send({
            message: "Une erreur est survenue lors de la suppression de la plante avec l'id=" + id
        });
    });
};

