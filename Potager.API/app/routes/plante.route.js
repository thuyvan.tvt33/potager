module.exports = app => {
const plantes = require("../controllers/plante.controller.js");

var router = require("express").Router();

//Créer d'une nouvelle plante
router.post("/", plantes.create);

//Récupérer toutes les plantes
router.get("/", plantes.getAll);

//Récupérer une plante grâce à son id
router.get("/:id", plantes.getById);

//Modifier une plante grâce à son id
router.put("/", plantes.update);

//Supprimer une plante grâce à son id
router.delete("/:id", plantes.delete);

app.use("/api/plantes", router);

};