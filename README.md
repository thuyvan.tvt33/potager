# Projet

Projet de gestion de potager basé sur Angular8 et NodeJS
Testé avec :
- npm en version 6.12.1
- node en version v12.16.1

## Lancer la solution

### Cloner le repo

```
git clone https://gitlab.com/thuyvan.tvt33/potager.git
cd Potager

```

### Démarrer le back (API NodeJS)
Ouvrir une première invite de commande à la racine du dossier cloné
Lancer l'installation des package et démarrer le projet

```
cd Potager.API
npm install
node server.js

```

le serveur écoute en port 8000

### Démarrer le front (Angular8)
Ouvrir une seconde invite de commande à la racine du dossier cloné
Lancer l'installation des package et démarrer le projet

```
cd Potager.Client
npm install
ng serve

```


## Détail de l'api

- Récupérer la liste des plantes
	- URL :**http://localhost:8000/api/plantes**
	- METHOD : *GET*
- Récupérer le détail d'une plante via son Id
	- URL :**http://localhost:8000/api/plantes/{id}**
	- METHOD : *GET*
- Supprimer une plante via son Id
	- URL :**http://localhost:8000/api/plantes/{id}**
	- METHOD : *DELETE*
- Ajouter une plante
	- URL :**http://localhost:8000/api/plantes**
	- Body : 
	{ 
        id: ,
        nom: 'Figue',
        type: 'Fruit',
        etat: 'Planté',
        dateMAJ: '18-05-2020',
        description: 'La figue est le fruit du figuier commun (Ficus carica) un arbre de la famille des moracées, emblème du bassin méditerranéen où il est cultivé depuis des millénaires. '
     }
	- METHOD : *POST*
- Modifier une plante
	- URL :**http://localhost:8000/api/plantes**
	- Body : 
	{ 
        id: 1,
        nom: 'Figue',
        type: 'Fruit',
        etat: 'Planté',
        dateMAJ: '18-05-2020'),
        description: 'La figue est le fruit du figuier commun (Ficus carica) un arbre de la famille des moracées, emblème du bassin méditerranéen où il est cultivé depuis des millénaires. '
     }
	- METHOD : *PUT