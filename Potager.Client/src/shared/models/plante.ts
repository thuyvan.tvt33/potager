export class Plante {
    id: number;
    nom: string;
    type: string;
    etat: string;
    dateMAJ: Date = new Date();
    description: string;
}