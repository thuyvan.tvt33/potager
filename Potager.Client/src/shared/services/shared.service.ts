import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

siteTitle = 'Mon petit Potager';
baseUrl = 'http://localhost:4200/';

constructor() { }

}
