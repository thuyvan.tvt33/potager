import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Plante } from '../models/plante';
import { PLANTES } from '../mock/plantes';

const baseUrl = 'http://localhost:8000/api/plantes/';

@Injectable({
  providedIn: 'root'
})
export class PlanteService {

  constructor(private http: HttpClient) { }

  getAllPlante(): Observable<Plante[]> {
    return this.http.get<Plante[]>(baseUrl);
  }

  getPlantebyId(idString: string): Observable<Plante> {
    return this.http.get<Plante>(baseUrl + idString);
  }

  create(data) {
    return this.http.post(baseUrl, data);
  }

  update(data) {
    return this.http.put(baseUrl, data);
  }

  delete(id) {
    // return this.http.delete(baseUrl, data);
    return this.http.delete(`${baseUrl}${id}`);
  }

}
