import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanteComponent } from './plante/plante.component';
import { PlanteListComponent } from './plante-list/plante-list.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AddPlanteComponent } from './add-plante/add-plante.component';


const routes: Routes = [
  {path: 'plantes', component: PlanteListComponent },
  {path: 'add', component: AddPlanteComponent },
  {path: 'update/:id', component: AddPlanteComponent },
  {path: '404', component: NotFoundComponent },
  {path: '', component: PlanteListComponent },
  {path: ':id', component: PlanteComponent },
  {path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
