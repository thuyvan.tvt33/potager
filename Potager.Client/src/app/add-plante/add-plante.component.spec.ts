import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPlanteComponent } from './add-plante.component';

describe('AddPlanteComponent', () => {
  let component: AddPlanteComponent;
  let fixture: ComponentFixture<AddPlanteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPlanteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPlanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
