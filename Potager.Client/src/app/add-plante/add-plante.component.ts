import { Component, OnInit } from '@angular/core';
import { PlanteService } from 'src/shared/services/plante.service';
import { Plante } from 'src/shared/models/plante';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-add-plante',
  templateUrl: './add-plante.component.html',
  styleUrls: ['./add-plante.component.css']
})
export class AddPlanteComponent implements OnInit {

 public plante = {
    id: null,
    nom: '',
    type: '',
    description: '',
    etat: 'Planté',
    dateMAJ: new Date()
  };
  public submitted = false;
  public isModification = false;
  public listType = ['Fruit', 'Légume'];
  public listEtat = ['Planté', 'Fleuri', 'Récolte'];

  constructor(private planteService: PlanteService,
              private router: Router,
              private route: ActivatedRoute ) { }

  ngOnInit(): void {

    this.route.params.subscribe(params => {
      const id = Number(params.id);
      if (params.id !== 'NaN') {
        if (id > 0) {
        this.planteService.getPlantebyId(params.id).subscribe(
        plante => {
          if (plante === undefined) {
            this.router.navigateByUrl('404');
            return;
          }
          this.isModification = true;
          this.plante = plante;
        }
      );
    }
    } else {
      this.nouvellePlante();
    }
    });
  }


  savePlante() {
    const data = {
      nom: this.plante.nom,
      type: this.plante.type,
      description: this.plante.description
    };

    this.planteService.create(data)
    .subscribe(
      response => {
        this.submitted = true;
      },
      error => {
        console.log(error);
      });
  }

  updatePlante() {
    this.planteService.update(this.plante)
    .subscribe(
      response => {
        this.submitted = true;
      },
      error => {
        console.log(error);
      });
  }

 changeEtat() {
   this.plante.dateMAJ = new Date ();
 }

 deletePlante() {
  this.planteService.delete(this.plante.id)
  .subscribe(
    response => {
      this.submitted = true;
    },
    error => {
      console.log(error);
    });
}

  nouvellePlante() {
    this.submitted = false;
    this.plante = {
      id: null,
      nom: '',
      type: '',
      description: '',
      etat: 'Planté',
      dateMAJ: new Date()
    };
  }

}
