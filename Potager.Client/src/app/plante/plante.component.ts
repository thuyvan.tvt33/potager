import { Component, OnInit } from '@angular/core';
import { Plante } from 'src/shared/models/plante';
import { ActivatedRoute, Router } from '@angular/router';
import { PlanteService } from 'src/shared/services/plante.service';
import { Title } from '@angular/platform-browser';
import { SharedService } from 'src/shared/services/shared.service';

@Component({
  selector: 'app-plante',
  templateUrl: './plante.component.html',
  styleUrls: ['./plante.component.scss']
})
export class PlanteComponent implements OnInit {

  plante: Plante = new Plante();

  constructor(private route: ActivatedRoute,
              private planteService: PlanteService,
              private router: Router,
              private titleService: Title,
              private sharedService: SharedService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = params.id;
      this.planteService.getPlantebyId(id).subscribe(
        plante => {
          if (plante === undefined) {
            this.router.navigateByUrl('404');
            return;
          }
          this.plante = plante;
          this.titleService.setTitle(
            `${this.plante.nom} - ${this.sharedService.siteTitle}`
          );

        }
      );
    });
  }

  updatePlante(): void {
    this.router.navigateByUrl('update/' + this.plante.id.toString());
    return;
  }

}
