import { Component, OnInit } from '@angular/core';
import { Plante } from 'src/shared/models/plante';
import { PlanteService } from 'src/shared/services/plante.service';
import { Title } from '@angular/platform-browser';
import { SharedService } from 'src/shared/services/shared.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-plante-list',
  templateUrl: './plante-list.component.html',
  styleUrls: ['./plante-list.component.scss']
})
export class PlanteListComponent implements OnInit {

  listOfPlantes: Plante[] = [];
  public listFiltre = ['Fruit', 'Légume', 'Tout'];
  public filtreSelected: string;
  public listPlanteFiltered: Plante[] = [];

  constructor(private planteService: PlanteService,
              private titleService: Title,
              private sharedService: SharedService,
              private router: Router) { }

  ngOnInit(): void {
    this.titleService.setTitle(` ${this.sharedService.siteTitle}`);
    this.getAllPlantes();
  }

  getAllPlantes(): void {
    this.planteService
    .getAllPlante()
    .subscribe(
      data => {
        this.listOfPlantes = data;
        this.listPlanteFiltered = this.listOfPlantes;
        console.log(data);
      },
      error => {
        console.log(error);
      }
      );
  }

  getPlanteInfo(id: number): void {
    this.router.navigateByUrl(id.toString());
    return;
  }

  refreshList(filtreSelected: string) {
    switch (filtreSelected) {
      case 'Tout':
        this.listPlanteFiltered = this.listOfPlantes;
        break;
      default:
        this.listPlanteFiltered = this.listOfPlantes.filter(x => x.type === filtreSelected);
        break;
    }
  }


}
